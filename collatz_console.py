import drulum

"""This is the reference test for the collatz package.
Run it from a console inside a virtualenv configured with the requirements.
"""


def collatz_reference():
    print('Enter the following information.')
    while True:
        try:
            start = int(input('Starting integer [1]: '))
        except ValueError:
            start = None
        if not start:
            start = 1
            break
        elif start != 0:
            break
        else:
            print('Start can not be 0!')

    while True:
        try:
            end = int(input('Range to test [10]: '))
        except ValueError:
            end = None
        if not end:
            end = 10
            break
        elif end > 0:
            break
        else:
            print('Range can not be 0!')

    # Simply does not work if anything other than a correctly formatted filename is entered. Design it first.
    # filename = input('Filename [YYYYMMDD_HHMMSS_dataset_<part>.txt]: ')

    print('\nFiles will be saved as: ./output/YYYYMMDD_HHMMSS_dataset_<part>.txt\n')
    print('Depending on the range you entered, this may take a long time!\n')
    print('Data is being generated...\n')

    c = drulum.collatz.conjecture.Collatz(start, end)  # , directory, filename)
    c.run()

    print('Data generation complete. Have a nice day!\n')


if __name__ == '__main__':
    collatz_reference()
