from datetime import datetime
import drulum


class Collatz:

    def __init__(self, start=1, runs=1, filename=None, directory='output'):
        self.start = start
        self.runs = runs
        self.directory = directory
        # FIXME: the following will only work for filename extensions of dot + 3 characters
        if filename:
            self.filename_pre = filename[:-4] + '_'
            self.filename_ext = filename[-4:]
        else:
            self.filename_pre = f'{datetime.now().strftime("%Y%m%d_%H%M%S")}_dataset_'
            self.filename_ext = '.txt'
        self.filename = ''
        self.file = None
        self.chunk_file = 1000000
        self.chunk_memory = 100000

    def run(self):
        next_start = self.start
        part = 1
        self.filename = self.filename_pre + str(part) + self.filename_ext

        remaining_runs = self.runs

        while remaining_runs:
            self.file_setup()
            if remaining_runs < self.chunk_file:
                file_remaining = remaining_runs
            else:
                file_remaining = self.chunk_file
            while file_remaining:
                if file_remaining >= self.chunk_memory:
                    self.collatz_chunk(next_start, self.chunk_memory)
                    next_start += self.chunk_memory
                    file_remaining -= self.chunk_memory
                else:
                    self.collatz_chunk(next_start, file_remaining)
                    next_start += file_remaining
                    file_remaining = 0
            if remaining_runs >= self.chunk_file:
                remaining_runs -= self.chunk_file
            else:
                remaining_runs = 0
            part += 1
            self.filename = self.filename_pre + str(part) + self.filename_ext

    def collatz_chunk(self, start, end):
        steps_set = []
        for i in range(start, (start + end)):
            step = i
            count = 0
            steps = [step]

            while step != 1:
                if step % 2 == 0:
                    step = int(step / 2)
                else:
                    step = int(step * 3 + 1)
                count += 1
                steps.append(step)

            steps_set.append(steps)

        self.file_store(steps_set)

    def file_setup(self):
        self.file = drulum.collatz.file.Store(self.directory, self.filename)
        self.file.create()

    def file_store(self, steps):
        self.file.store(steps)
