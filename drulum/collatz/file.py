import os


class Store:

    def __init__(self, directory, filename):
        # Need to move the output directory to conjecture.py init for configuration
        self.directory = directory
        self.filename = f'{self.directory}/{filename}'

    def create(self):
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        file = open(self.filename, 'w')
        file.close()

    def store(self, steps):
        file = open(self.filename, 'a')
        for i in steps:
            file.write(f'{i}\n')
        file.close()
